package org.example.repository;

import org.example.domain.CustomerAccount;
import org.example.domain.Purchase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerAccountRepository extends CrudRepository<CustomerAccount, Long> {
    List<CustomerAccount> findAllCustomersByFirstName(String firstName);

    List<CustomerAccount> findAll();

    List<CustomerAccount> findAllByLogin(String login);
}
