package org.example.dto.mapper;

import org.example.domain.Purchase;
import org.example.dto.PurchaseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PurchaseMapper implements Mapper<PurchaseDto, Purchase> {

    private final ProductMapper productMapper;

    @Autowired
    public PurchaseMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    @Override
    public Purchase mapping(PurchaseDto purchaseDto) {
        Purchase purchase = new Purchase();
        purchase.setDate(purchaseDto.getDate());
        purchase.setCost(purchaseDto.getCost());
        purchase.setProducts(productMapper.mappingAll(purchaseDto.getProducts()));

        return purchase;
    }

    @Override
    public PurchaseDto unMapping(Purchase purchase) {
        PurchaseDto purchaseDto = new PurchaseDto();
        purchaseDto.setCost(purchase.getCost());
        purchaseDto.setDate(purchase.getDate());
        purchaseDto.setId(purchase.getId());

        purchaseDto.setProducts(productMapper.unMappingAll(purchase.getProducts()));

        return purchaseDto;
    }
}