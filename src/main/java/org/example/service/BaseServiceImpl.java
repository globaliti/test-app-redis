package org.example.service;

import org.springframework.data.repository.CrudRepository;

import java.util.LinkedList;
import java.util.List;

public abstract class BaseServiceImpl<E, ID> implements BaseService<E,ID>{
    public abstract CrudRepository<E, ID> getRepository();

    @Override
    public E get(ID id) {
        return getRepository().findById(id).orElse(null);
    }

    @Override
    public E save(E entity) {
        return getRepository().save(entity);
    }

    @Override
    public void delete(E entity) {
        getRepository().delete(entity);
    }

    @Override
    public List<E> findAll() {
        List<E> allEntities = new LinkedList<>();
        getRepository().findAll().forEach(allEntities::add);
        return allEntities;
    }
}