package org.example.validation;

import org.example.dto.CreateCustomerAccountDto;
import org.example.exception.AccountValidationException;
import org.example.service.CustomerAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CreateCustomerAccountValidator implements Validator {

    @Autowired
    CustomerAccountService customerAccountService;

    @Override
    public boolean supports(Class<?> clazz) {
        return CreateCustomerAccountDto.class == clazz;
    }

    @Override
    public void validate(Object target, Errors errors) {
        CreateCustomerAccountDto customerAccount = (CreateCustomerAccountDto) target;

        if (customerAccountService.findByLogin(customerAccount.getLogin()) != null) {
            throw new AccountValidationException("Account with this login already exists");
        }
    }
}