package org.example.exception;

public class EntityStructureException extends RuntimeException {
    public EntityStructureException() {
        super();
    }

    public EntityStructureException(String message) {
        super(message);
    }

    public EntityStructureException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityStructureException(Throwable cause) {
        super(cause);
    }

    protected EntityStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
