package org.example.dto.mapper;

import org.example.builder.domain.ProductBuilder;
import org.example.builder.domain.PurchaseBuilder;
import org.example.builder.dto.ProductDtoBuilder;
import org.example.builder.dto.PurchaseDtoBuilder;
import org.example.domain.Purchase;
import org.example.dto.PurchaseDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.example.builder.domain.ProductBuilder.product;
import static org.example.builder.dto.ProductDtoBuilder.productDto;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PurchaseMapperTest {

    @Mock
    ProductMapper productMapper;

    @InjectMocks
    PurchaseMapper purchaseMapper;

    @Test
    public void mapping_whenPurchaseWithoutId_purchase() {
        ProductDtoBuilder productDtoBuilder1 = productDto()
                .setCost(50D)
                .setName("product1")
                .setDescription("test description for prod 1 ")
                .setId(15L);
        ProductDtoBuilder productDtoBuilder2 = productDto()
                .setCost(50D)
                .setName("product2")
                .setDescription("test description for prod 2 ")
                .setId(30L);
        PurchaseDto purchaseDto = PurchaseDtoBuilder.purchaseDto()
                .setCost(100D)
                .setDate(LocalDateTime.of(2019, 2, 15, 12, 22, 22))
                .addProducts(productDtoBuilder1)
                .addProducts(productDtoBuilder2)
                .build();

        ProductBuilder productBuilder1 = product()
                .setCost(50D)
                .setName("product1")
                .setDescription("test description for prod 1 ")
                .setId(15L);
        ProductBuilder productBuilder2 = product()
                .setCost(50D)
                .setName("product2")
                .setDescription("test description for prod 2 ")
                .setId(30L);
        Purchase expectedPurchase = PurchaseBuilder.purchase()
                .setCost(100D)
                .setDate(LocalDateTime.of(2019, 2, 15, 12, 22, 22))
                .addProducts(productBuilder1)
                .addProducts(productBuilder2)
                .build();

        given(productMapper.mappingAll(eq(Arrays.asList(productDtoBuilder1.build(), productDtoBuilder2.build()))))
                .willReturn(Arrays.asList(productBuilder1.build(), productBuilder2.build()));

        Purchase mappedPurchase = purchaseMapper.mapping(purchaseDto);

        Assert.assertNotNull(mappedPurchase);
        Assert.assertEquals(expectedPurchase, mappedPurchase);
    }

    @Test
    public void mapping_whenPurchaseWithId_purchase() {
        ProductDtoBuilder firstProductDto = productDto()
                .setCost(50D)
                .setName("product1")
                .setDescription("test description for prod 1 ")
                .setId(15L);
        ProductDtoBuilder secondProductDto = productDto()
                .setCost(50D)
                .setName("product2")
                .setDescription("test description for prod 2 ")
                .setId(30L);

        ProductBuilder firstProduct = product()
                .setCost(50D)
                .setName("product1")
                .setDescription("test description for prod 1 ")
                .setId(15L);
        ProductBuilder secondProduct = product()
                .setCost(50D)
                .setName("product2")
                .setDescription("test description for prod 2 ")
                .setId(30L);
        PurchaseDto purchaseDto = PurchaseDtoBuilder.purchaseDto()
                .setCost(100D)
                .setDate(LocalDateTime.of(2019, 2, 15, 12, 22, 22))
                .addProducts(firstProductDto)
                .addProducts(secondProductDto)
                .build();

        Purchase expectedPurchase = PurchaseBuilder.purchase()
                .setCost(100D)
                .setDate(LocalDateTime.of(2019, 2, 15, 12, 22, 22))
                .addProducts(firstProduct)
                .addProducts(secondProduct)
                .build();

        given(productMapper
                .mappingAll(
                        Arrays.asList(
                                firstProductDto.build(),
                                secondProductDto.build())))
                .willReturn(Arrays.asList(
                        firstProduct.build(),
                        secondProduct.build()));

        Purchase mappedPurchase = purchaseMapper.mapping(purchaseDto);

        Assert.assertNotNull(mappedPurchase);
        Assert.assertEquals(expectedPurchase, mappedPurchase);
    }

    @Test
    public void unMapping() {
        ProductDtoBuilder productDtoBuilder1 = productDto()
                .setCost(50D)
                .setName("product1")
                .setDescription("test description for prod 1 ")
                .setId(15L);
        ProductDtoBuilder productDtoBuilder2 = productDto()
                .setCost(50D)
                .setName("product2")
                .setDescription("test description for prod 2 ")
                .setId(30L);

        PurchaseDto expectedPurchaseDto = PurchaseDtoBuilder.purchaseDto()
                .setCost(100D)
                .setId("testId")
                .setDate(LocalDateTime.of(2019, 2, 15, 12, 22, 22))
                .addProducts(productDtoBuilder1)
                .addProducts(productDtoBuilder2)
                .build();

        ProductBuilder productBuilder1 = product()
                .setCost(50D)
                .setName("product1")
                .setDescription("test description for prod 1 ")
                .setId(15L);
        ProductBuilder productBuilder2 = product()
                .setCost(50D)
                .setName("product2")
                .setDescription("test description for prod 2 ")
                .setId(30L);

        Purchase purchase = PurchaseBuilder.purchase()
                .setCost(100D)
                .setId("testId")
                .setDate(LocalDateTime.of(2019, 2, 15, 12, 22, 22))
                .addProducts(productBuilder1)
                .addProducts(productBuilder2)
                .build();

        given(productMapper.unMappingAll(eq(Arrays.asList(productBuilder1.build(), productBuilder2.build()))))
                .willReturn(Arrays.asList(productDtoBuilder1.build(), productDtoBuilder2.build()));

        PurchaseDto unMappedPurchaseDto = purchaseMapper.unMapping(purchase);

        Assert.assertNotNull(unMappedPurchaseDto);
        Assert.assertEquals(expectedPurchaseDto, unMappedPurchaseDto);
    }
}