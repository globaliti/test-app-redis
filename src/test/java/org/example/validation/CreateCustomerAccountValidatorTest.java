package org.example.validation;

import org.example.builder.domain.CustomerAccountBuilder;
import org.example.dto.CreateCustomerAccountDto;
import org.example.exception.AccountValidationException;
import org.example.service.CustomerAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class CreateCustomerAccountValidatorTest {

    @Mock
    CustomerAccountService customerAccountService;

    @InjectMocks
    CreateCustomerAccountValidator createCustomerAccountValidator;

    @Test(expected = AccountValidationException.class)
    public void validate_whenAccountWithTestLoginAlreadyExists_throwAccountValidationException() {
        Errors errors = Mockito.mock(Errors.class);
        CreateCustomerAccountDto createCustomerAccountDto = new CreateCustomerAccountDto();
        createCustomerAccountDto.setLogin("testLogin");

        given(customerAccountService.findByLogin(eq("testLogin")))
                .willReturn(CustomerAccountBuilder.customer()
                        .setLogin("testLogin")
                        .build());

        createCustomerAccountValidator.validate(createCustomerAccountDto, errors);
    }

    @Test
    public void validate_withNewLoginThatNotExistsInDB() {
        Errors errors = Mockito.mock(Errors.class);
        CreateCustomerAccountDto createCustomerAccountDto = new CreateCustomerAccountDto();
        createCustomerAccountDto.setLogin("testLogin");

        createCustomerAccountValidator.validate(createCustomerAccountDto, errors);
    }
}