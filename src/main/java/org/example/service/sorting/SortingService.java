package org.example.service.sorting;

import org.example.domain.Product;

import java.util.List;

public interface SortingService {
    <T> List<T> sort(List<T> list, Comparator<? super T> comparator);
}
