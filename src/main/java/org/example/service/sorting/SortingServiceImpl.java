package org.example.service.sorting;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SortingServiceImpl implements SortingService {

    @Override
    public <T> List<T> sort(List<T> list, Comparator<? super T> comparator) {
        quickSort(list, 0, list.size() - 1, comparator);
        return list;
    }

    <T> int partition(List<T> list, int begin, int end, Comparator<? super T> comparator) {
        int pivot = end;

        int counter = begin;
        for (int i = begin; i < end; i++) {
            if (comparator.compare(list.get(i), list.get(pivot)) == CompareResult.LESS) {
                swapElementsInList(list, counter, i);
                counter++;
            }
        }

        swapElementsInList(list, counter, pivot);
        return counter;
    }

    <T> void swapElementsInList(List<T> list, int firstIndex, int secondIndex) {
        if (firstIndex == secondIndex) {
            return;
        }

        T temp = list.get(firstIndex);
        list.set(firstIndex, list.get(secondIndex));
        list.set(secondIndex, temp);
    }

    <T> void quickSort(List<T> list, int begin, int end, Comparator<? super T> comparator) {
        if (end <= begin) return;
        int pivot = partition(list, begin, end, comparator);
        quickSort(list, begin, pivot - 1, comparator);
        quickSort(list, pivot + 1, end, comparator);
    }


    <T> List<T> bobbleSort(List<T> list, Comparator<? super T> comparator) {
        boolean wasSorting;
        do {
            wasSorting = false;
            for (int i = 0; i < (list.size() - 1); i++) {
                if (comparator.compare(list.get(i), list.get(i + 1)) == CompareResult.GREATER) {
                    T swapElement = list.get(i);
                    list.set(i, list.get(i + 1));
                    list.set(i + 1, swapElement);
                    wasSorting = true;
                }
            }
        } while (wasSorting);

        return list;
    }
}
