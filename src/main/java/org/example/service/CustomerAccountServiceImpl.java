package org.example.service;

import org.example.builder.domain.CustomerAccountBuilder;
import org.example.domain.CustomerAccount;
import org.example.domain.Product;
import org.example.domain.Purchase;
import org.example.dto.CreateCustomerAccountDto;
import org.example.exception.DuplicationUniqueFieldException;
import org.example.exception.EntityNotFoundException;
import org.example.repository.CustomerAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class CustomerAccountServiceImpl extends BaseServiceImpl<CustomerAccount, Long> implements CustomerAccountService {

    private final CustomerAccountRepository customerAccountRepository;

    private final CryptoService cryptoService;


    @Autowired
    public CustomerAccountServiceImpl(
            CustomerAccountRepository customerAccountRepository,
            CryptoService cryptoService) {
        this.customerAccountRepository = customerAccountRepository;
        this.cryptoService = cryptoService;
    }

    @Override
    public CrudRepository<CustomerAccount, Long> getRepository() {
        return customerAccountRepository;
    }

    @Override
    public CustomerAccount registerNewAccount(CreateCustomerAccountDto createCustomerAccountDto) {
        CustomerAccount customerAccount = null;
        if (customerAccountRepository.findAllByLogin(createCustomerAccountDto.getLogin()) == null
                || customerAccountRepository.findAllByLogin(createCustomerAccountDto.getLogin()).isEmpty()) {
            customerAccount = mappedToCustomerAccount(createCustomerAccountDto);
            save(customerAccount);
        }
        return customerAccount;
    }

    private CustomerAccount mappedToCustomerAccount(CreateCustomerAccountDto createCustomerAccountDto) {
        return CustomerAccountBuilder.customer()
                .setLogin(createCustomerAccountDto.getLogin())
                .setFirstName(createCustomerAccountDto.getFirstName())
                .setLastName(createCustomerAccountDto.getLastName())
                .setEmail(createCustomerAccountDto.getEmail())
                .setPasswordHash(cryptoService.createHashForPassword(createCustomerAccountDto.getPassword()))
                .build();
    }

    @Override
    public CustomerAccount findByLogin(String login) {
        List<CustomerAccount> accounts = customerAccountRepository.findAllByLogin(login);
        if (accounts.size() > 1) {
            throw new DuplicationUniqueFieldException("Found two account with same login");
        }

        if (!accounts.isEmpty()) {
            return accounts.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Purchase addPurchase(Long customerId, Purchase purchase) {
        CustomerAccount customerAccount = get(customerId);

        if (customerAccount != null) {
            generateRequiredFields(purchase);
            if (customerAccount.getPurchases() != null) {
                customerAccount.getPurchases().add(purchase);
            } else {
                customerAccount.setPurchases(Collections.singletonList(purchase));
            }
            save(customerAccount);
            return purchase;
        } else {
            return null;
        }
    }

    @Override
    @Nullable
    public Purchase findPurchaseInCustomerPurchases(Long customerId, String purchaseId) {
        CustomerAccount customerAccount = get(customerId);
        if (customerAccount != null) {
            List<Purchase> customerPurchases = customerAccount.getPurchases();
            if (customerPurchases != null) {
                for (Purchase purchase : customerPurchases) {
                    if (purchase.getId().equals(purchaseId)) {
                        return purchase;
                    }
                }
            }
        }
        return null;
    }

    private void generateRequiredFields(Purchase purchase) {
        if (purchase.getId() == null) {
            purchase.setId(UUID.randomUUID().toString());
        }
        if (purchase.getDate() == null) {
            purchase.setDate(LocalDateTime.now());
        }
        if (purchase.getCost() == null) {
            Double cost = 0D;
            for (Product product : purchase.getProducts()) {
                cost += product.getCost();
            }
            purchase.setCost(cost);
        }
    }

    @Override
    public void deletePurchase(Long customerId, String purchaseId) throws EntityNotFoundException {
        if (findPurchaseInCustomerPurchases(customerId, purchaseId) == null) {
            throw new EntityNotFoundException("Purchase was not found");
        }

        CustomerAccount customerAccount = get(customerId);
        if (customerAccount.getPurchases() != null) {
            List<Purchase> purchases = customerAccount.getPurchases();
            if (purchases != null) {
                purchases.removeIf(purchase -> purchase.getId().equals(purchaseId));
                customerAccount.setPurchases(purchases);
                save(customerAccount);
            }
        }
    }
}