package org.example.dto.mapper;

import org.example.builder.domain.ProductBuilder;
import org.example.builder.dto.ProductDtoBuilder;
import org.example.domain.Product;
import org.example.dto.ProductDto;
import org.example.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.example.builder.domain.ProductBuilder.product;
import static org.example.builder.dto.ProductDtoBuilder.productDto;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProductMapperTest {

    @Mock
    ProductService productService;

    @InjectMocks
    ProductMapper productMapper;

    @Test
    public void mapping_whenProductWithoutId_newProduct() {
        ProductDto productDto = new ProductDto();
        productDto.setName("TestName");
        productDto.setDescription("TestDescription");
        productDto.setCost(1500D);

        Product product = productMapper.mapping(productDto);

        Assert.assertNotNull(product);
        Assert.assertEquals(productDto.getName(), product.getName());
        Assert.assertEquals(productDto.getDescription(), product.getDescription());
        Assert.assertEquals(productDto.getCost(), product.getCost());
    }

    @Test
    public void mapping_whenProductWithId_productGottenFromService() {
        Long productId = 1515L;
        ProductDto productDto = productDto()
                .setId(productId)
                .setName("TestName")
                .setDescription("TestDescription")
                .setCost(1500D)
                .build();

        Product productForService = product()
                .setName("TestName")
                .setDescription("TestDescription")
                .setCost(1500D)
                .setId(productId)
                .build();

        given(productService.get(eq(productId))).willReturn(productForService);

        Product mappedProduct = productMapper.mapping(productDto);

        Assert.assertNotNull(mappedProduct);
        Assert.assertEquals(productDto.getId(), mappedProduct.getId());
        Assert.assertEquals(productDto.getName(), mappedProduct.getName());
        Assert.assertEquals(productDto.getDescription(), mappedProduct.getDescription());
        Assert.assertEquals(productDto.getCost(), mappedProduct.getCost());

        verify(productService).get(eq(productId));
    }

    @Test
    public void unMapping() {
        Long productId = 1515L;
        ProductDto expectedProductDto = productDto()
                .setId(productId)
                .setName("TestName")
                .setDescription("TestDescription")
                .setCost(1500D)
                .build();

        Product sourceProduct = product()
                .setName("TestName")
                .setDescription("TestDescription")
                .setCost(1500D)
                .setId(productId)
                .build();

        ProductDto unMappedProductDto = productMapper.unMapping(sourceProduct);

        Assert.assertNotNull(unMappedProductDto);
        Assert.assertEquals(expectedProductDto, unMappedProductDto);
    }

    @Test
    public void mappingAll_objectsWithId() {
        List<ProductDto> productDtos = new LinkedList<>();
        ProductDtoBuilder productDtoBuilder = ProductDtoBuilder.productDto()
                .setName("name1")
                .setDescription("description2")
                .setCost(50D);
        ProductBuilder productBuilder = product()
                .setName("name1")
                .setDescription("description2")
                .setCost(50D);

        productDtoBuilder.setId(1L);
        productBuilder.setId(1L);
        productDtos.add(productDtoBuilder.build());
        given(productService.get(eq(Long.valueOf(1L)))).willReturn(productBuilder.build());

        productDtoBuilder.setId(2L);
        productBuilder.setId(2L);
        productDtos.add(productDtoBuilder.build());
        given(productService.get(eq(Long.valueOf(2L)))).willReturn(productBuilder.build());

        productDtoBuilder.setId(3L);
        productBuilder.setId(3L);
        productDtos.add(productDtoBuilder.build());
        given(productService.get(eq(Long.valueOf(3L)))).willReturn(productBuilder.build());

        List<Product> mappedProducts = productMapper.mappingAll(productDtos);

        Assert.assertNotNull(mappedProducts);
        Assert.assertFalse(mappedProducts.isEmpty());
        Assert.assertEquals(3, mappedProducts.size());
    }
}